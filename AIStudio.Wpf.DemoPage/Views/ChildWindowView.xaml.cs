﻿using System.Windows.Controls;

namespace AIStudio.Wpf.DemoPage.Views
{
    /// <summary>
    /// ChildWindowView.xaml 的交互逻辑
    /// </summary>
    public partial class ChildWindowView : UserControl
    {
        public ChildWindowView()
        {
            InitializeComponent();
        }
    }
}
