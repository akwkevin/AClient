﻿namespace AIStudio.Wpf.DemoPage.Models
{
    public class StepBarDemoModel
    {
        public string Header { get; set; }

        public string Content { get; set; }
    }
}