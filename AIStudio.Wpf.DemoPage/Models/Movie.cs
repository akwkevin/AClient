﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStudio.Wpf.DemoPage.Models
{
    public class Movie
    {
        public string Title
        {
            get;
            set;
        }

        public string Review
        {
            get;
            set;
        }

        public double Rating
        {
            get;
            set;
        }
    }
}
