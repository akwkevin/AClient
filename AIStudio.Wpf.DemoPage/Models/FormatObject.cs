﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStudio.Wpf.DemoPage.Models
{
    public class FormatObject
    {
        public string Value
        {
            get;
            set;
        }

        public string DisplayValue
        {
            get;
            set;
        }
    }
}
