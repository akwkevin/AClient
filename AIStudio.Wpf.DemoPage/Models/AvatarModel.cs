﻿
namespace AIStudio.Wpf.DemoPage.Models
{
    public class AvatarModel
    {
        public string DisplayName { get; set; }

        public string Link { get; set; }

        public string AvatarUri { get; set; }
    }
}