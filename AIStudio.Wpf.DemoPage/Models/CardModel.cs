﻿namespace AIStudio.Wpf.DemoPage.Models
{
    public class CardModel
    {
        public string Header { get; set; }

        public string Content { get; set; }

        public string Footer { get; set; }
    }
}
