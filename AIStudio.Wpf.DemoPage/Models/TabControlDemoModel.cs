﻿namespace AIStudio.Wpf.DemoPage.Models
{
    public class TabControlDemoModel
    {
        public string Header { get; set; }

        public string BackgroundToken { get; set; }
    }
}
