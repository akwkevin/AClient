﻿using System;
using System.Collections.Generic;



namespace AIStudio.Wpf.EFCore.Models
{
    public partial class Base_PermissionAppId
    {
        public string Id { get; set; }
        public string AppId { get; set; }
        public string PermissionValue { get; set; }
    }
}
