﻿using System;
using System.Collections.Generic;



namespace AIStudio.Wpf.EFCore.Models
{
    public partial class Base_PermissionRole
    {
        public string Id { get; set; }
        public string RoleId { get; set; }
        public string PermissionValue { get; set; }
    }
}
