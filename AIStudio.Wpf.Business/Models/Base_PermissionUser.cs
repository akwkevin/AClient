﻿using System;
using System.Collections.Generic;



namespace AIStudio.Wpf.EFCore.Models
{
    public partial class Base_PermissionUser
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string PermissionValue { get; set; }
    }
}
