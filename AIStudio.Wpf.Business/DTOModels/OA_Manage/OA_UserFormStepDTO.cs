﻿using AIStudio.Wpf.EFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStudio.Wpf.Business.DTOModels
{
    public class OA_UserFormStepDTO : OA_UserFormStep
    {
        public string Avatar { get; set; }
    }
}
