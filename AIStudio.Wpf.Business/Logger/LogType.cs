﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.Business
{
    /// <summary>
    /// 系统日志类型
    /// </summary>
    public enum LogType
    {
        系统异常,
        系统跟踪,
        WebSocket,
    }
}
